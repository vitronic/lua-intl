LUA     = 5.4
PREFIX  = /usr
LIBDIR  = $(PREFIX)/lib/lua/$(LUA)
MSGFMT  = /usr/bin/msgfmt
LOCALES = ./locales
LANGS   = es fr zh

LUA_CFLAGS  = $(shell pkg-config --cflags lua$(LUA))
CFLAGS  = -fPIC $(LUA_CFLAGS) -I/usr/include/
LIBS    = $(shell pkg-config --libs lua$(LUA)) -lintl

intl.so: lua-intl.o
	$(CC) -shared $(CFLAGS) -o $@ lua-intl.o $(LIBS) -lm

install:
	mkdir -p $(DESTDIR)$(LIBDIR)
	cp intl.so $(DESTDIR)$(LIBDIR)

locale:
	for i in $(LANGS); do \
		$(MSGFMT) --check-accelerators=_ $(LOCALES)/$$i/LC_MESSAGES/messages.po \
		-o $(LOCALES)/$$i/LC_MESSAGES/messages.mo; \
	done

docs: intl.so docs/config.ld
	ldoc -c docs/config.ld -d html -a .

clean:
	rm -rf *.o *.so
	for i in $(LANGS); do \
		rm $(LOCALES)/$$i/LC_MESSAGES/messages.mo; \
	done


.PHONY: intl.so
