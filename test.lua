#!/usr/bin/env lua5.4

--[[
 @filename  test.lua
 @version   1.0
 @autor     Máster Vitronic <mastervitronic@gmail.com>
 @date      mar oct 24 10:02:34 -04 2023
 @licence   MIT licence
]]--

local intl = require("intl")

--intl.setlocale("LC_ALL","es_ES.UTF-8")
--intl.setlocale("LANGUAGE","fr_FR")
--intl.setlocale("LC_ALL","")
--intl.setlocale("LANGUAGE","es_ES.UTF-8")
--intl.setlocale("LC_ALL","en_US.UTF-8")
--intl.setlocale("LC_MESSAGES","es_ES.UTF-8")

intl.setlocale("LANGUAGE","fr_FR")
intl.bindtextdomain("messages", "./locales")
intl.textdomain("messages")
local gettext = intl.gettext
local _ = gettext


print(_("Hello World"))
print(_("Good night"))
print(_("Amount"))
print(gettext("YES"))


print( os.getenv('LANGUAGE') )

