/*

  lua-intl.c - Lua bindings to gettext

  Copyright (c) 2023, Díaz Devera Víctor <mastervitronic@gmail.com>

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/***
 * Lua bindings to gettext
 *
 * This documentation is partial, and doesn't cover all functionality yet.
 * @module intl
 * @author Díaz Devera Víctor (Máster Vitronic) <mastervitronic@gmail.com>
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include "compat.h"
#include <libintl.h>
#include <locale.h>
#define _(STRING) gettext(STRING)

#define INTL "INTL"

//typedef struct ctx_t{
  //lua_State *L ;
//} ctx_t;

//static ctx_t * ctx_check(lua_State *L, int i) {
	//return (ctx_t *) luaL_checkudata(L, i, INTL);
//}

//static int l_new (lua_State *L) {
  //ctx_t *ctx = (ctx_t *)lua_newuserdata(L, sizeof(ctx_t));
  //ctx->L = L;
  //luaL_getmetatable(L, INTL);
  //lua_setmetatable(L, -2);
  //return 1;
//}

/***
 * Look up msgid in the current default message catalog for the current
 * LC_MESSAGES locale.  If not found, returns msgid itself (the default
 * text)
 * @function gettext
 * @tparam string msgid
 * @treturn string result
 */
static int l_gettext(lua_State * L) {
    const char * msgid  = luaL_checkstring(L, 1);

    lua_pushstring(L,  _(msgid));
    return 1;
}

/***
 * Similar to 'gettext' but select the plural form corresponding to the
 * number count. 
 * @function ngettext
 * @tparam string single
 * @tparam string plural
 * @tparam int count
 * @treturn string result
 */
static int l_ngettext(lua_State * L) {
    const char * single  = luaL_checkstring(L, 1);
    const char * plural  = luaL_checkstring(L, 2);
    const int count      = luaL_checknumber(L, 3);

    lua_pushstring(L,  ngettext(single,plural,count));
    return 1;
}

/***
 * Support for the locale chosen by the user. 
 * @tparam string category
 * @tparam string locale
 * @function setlocale
 */
static int l_setlocale(lua_State * L) {
    const char * category = luaL_checkstring(L, 1);
    const char * locale   = luaL_checkstring(L, 2);
    int number;
    if (strcmp(category, "LC_CTYPE") == 0) {
        number = 0;
    } else if (strcmp(category, "LC_NUMERIC") == 0) {
        number = 1;
    } else if (strcmp(category, "LC_TIME") == 0) {
        number = 2;
    } else if (strcmp(category, "LC_COLLATE") == 0) {
        number = 3;
    } else if (strcmp(category, "LC_MONETARY") == 0) {
        number = 4;
    } else if (strcmp(category, "LC_MESSAGES") == 0) {
        number = 5;
    } else if (strcmp(category, "LC_ALL") == 0) {
      number = 6;
    }

    setenv(category,locale,1);
    setlocale(number, locale);
    return 1;
}


/***
 * Specify that the domainname message catalog will be found
 * in dirname rather than in the system locale data base.
 * @tparam string domainname
 * @tparam string dirname
 * @function bindtextdomain
 */
static int l_bindtextdomain(lua_State * L) {
    const char * domainname = luaL_checkstring(L, 1);
    const char * dirname    = luaL_checkstring(L, 2);

    bindtextdomain(domainname, dirname);
    return 1;
}

/***
 * Set the current default message catalog to domainname.
 * If domainname is null, return the current default.
 * If domainname is "", reset to the default of "messages". 
 * @tparam string domainname
 * @function textdomain
 */
static int l_textdomain(lua_State * L) {
    const char * domainname  = luaL_checkstring(L, 1);
    if (strcmp(domainname, "nil") == 0){
      domainname = NULL;
    }

    textdomain(domainname);
    return 1;
}


  //{"new", l_new},
static const struct luaL_Reg funcs [] = {
  {"gettext", l_gettext},
  {"ngettext", l_ngettext},
  {"setlocale", l_setlocale},
  {"bindtextdomain", l_bindtextdomain},
  {"textdomain", l_textdomain},
  {NULL, NULL}
};

static const struct luaL_Reg meths [] = {
  {NULL, NULL}
};

int luaopen_intl (lua_State *L) {
  luaL_newmetatable(L, INTL);
  lua_pushvalue(L, -1);

  lua_setfield(L, -2, "__index");
  luaL_setfuncs(L, meths, 0);

  luaL_newlib(L, funcs);

  return 1;
}
