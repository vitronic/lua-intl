## lua-intl: Lua bindings for GNU gettext

lua-intl is a Lua binding library for [GNU gettext](https://www.gnu.org/software/gettext).

It runs on GNU/Linux and requires [Lua](http://www.lua.org/) (>=5.1)

_Authored by:_ _[Díaz Devera Víctor Diex Gamar (Máster Vitronic)](https://www.linkedin.com/in/Master-Vitronic)_

[![Lua logo](./docs/powered-by-lua.gif)](http://www.lua.org/)

#### License

GPL license . See [LICENSE](./LICENSE).

#### Documentation

See the [Reference Manual](https://vitronic.gitlab.io/lua-intl/).



#### Getting and installing

```sh
$ git clone https://gitlab.com/vitronic/lua-intl.git
$ cd lua-intl
lua-intl$ make
lua-intl$ make install # or 'sudo make install' (Ubuntu)
```

#### Example

```lua
--- Script: test.lua

intl.setlocale("LANGUAGE","es_ES")
intl.bindtextdomain("messages", "./locales")
intl.textdomain("messages")
local gettext = intl.gettext
local _ = gettext


print(_("Hello World"))
print(_("Good night"))
print(_("Amount"))
print(gettext("YES"))

```

The script can be executed at the shell prompt with the standard Lua interpreter:

```shell
$ LC_ALL=es_ES lua test.lua
Hola Mondo
Buenas noches
Monto
SI
```

```shell
$ LC_ALL=fr_FR lua test.lua
Bonjour le monde
Bonne nuit
Montant
OUI
```

```shell
$ LC_ALL=zh_CN lua test.lua
你好，世界
晚上好
数额
是

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
